Date: 14-Feb-20
Change Reprocessing Yield Calculation in Calculate Method in Packing List Class File

Date: 2/18/2020
In Issued Item Reprocessing Net Weight Total incorrect (Ticket #23995)

Date: 2/18/2020
In Reprocessing FG Issue, add new line to be automatic (Ticket #24000)

Date: 2/18/2020
Remarks field or User defined fields in Packing Items Screen (Ticket #24027)

Date: 2/18/2020
Under Received Items Tab There Is A Field Called Packing Issue. Golden Arrow Facilities Need To Be Provided For This Field in Packing & Unpacking (Ticket #24026)

Add : changes : date: 23/02/2020
Landing reciept screen - spot combobox change to textfield.

AUTAL DATE: 2/19/2020  AND COM - DATE: 26/02/2020
Multiple LR to be chosen with multiple line items (Ticket #23997)

Date: 28/2/2020
In multiple production, batch selection auto selection required (Ticket #23998)

Date: 11/3/2020   
Relationship Map with GRPO & its Production (Goods Receipt & Goods Issue) (Ticket #24002)
